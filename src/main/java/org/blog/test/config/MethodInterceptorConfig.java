package org.blog.test.config;

import org.blog.test.service.TestService;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class MethodInterceptorConfig {

    @Autowired
    private TestService testService;

    @Bean
    @Primary
    public ProxyFactoryBean testProxyFactoryBean() {
        ProxyFactoryBean testProxyFactoryBean = new ProxyFactoryBean();
        testProxyFactoryBean.setTarget(testService);
        testProxyFactoryBean.setInterceptorNames("testServiceInterceptor");
        return testProxyFactoryBean;
    }
}
