package org.blog.test;

import org.blog.test.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MethodInterceptorApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(MethodInterceptorApplication.class, args);
    }

    @Autowired
    private TestService testService;

    @Override
    public void run(String... args) throws Exception {
        testService.testMethod();
    }
}