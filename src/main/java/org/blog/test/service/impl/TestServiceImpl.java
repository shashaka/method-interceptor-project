package org.blog.test.service.impl;

import org.blog.test.service.TestService;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {
    @Override
    public void testMethod() {
        System.out.println("testService String");
    }
}
